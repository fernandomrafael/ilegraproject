package br.com.ilegra.model;

import java.io.Serializable;
import java.util.ArrayList;

public class Sales implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 804457106254032141L;
	
	private Integer salesId;
	
	private ArrayList<Item> itens;
	
	private Salesman salesman;

	/**
	 * @return the saleId
	 */
	public Integer getSalesId() {
		return salesId;
	}

	/**
	 * @param saleId the saleId to set
	 */
	public void setSalesId(Integer salesId) {
		this.salesId = salesId;
	}

	/**
	 * @return the itens
	 */
	public ArrayList<Item> getItens() {
		return itens;
	}

	/**
	 * @param itens the itens to set
	 */
	public void setItens(ArrayList<Item> itens) {
		this.itens = itens;
	}

	/**
	 * @return the salesman
	 */
	public Salesman getSalesman() {
		return salesman;
	}

	/**
	 * @param salesman the salesman to set
	 */
	public void setSalesman(Salesman salesman) {
		this.salesman = salesman;
	}
	
	
}
	
	
	
