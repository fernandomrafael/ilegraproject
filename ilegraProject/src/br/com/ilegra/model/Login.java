package br.com.ilegra.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "tb_user")
public class Login {
	
	
	@Id
	@SequenceGenerator(name = "tb_user_id_user_seq", sequenceName = "tb_user_id_user_seq")
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "tb_user_id_user_seq")
	@Column(name = "id_user")
	private Integer idUsuario;
	
	private String nmLogin;
	
	private String cdSenha;

	public Integer getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getNmLogin() {
		return nmLogin;
	}

	public void setNmLogin(String nmLogin) {
		this.nmLogin = nmLogin;
	}

	public String getCdSenha() {
		return cdSenha;
	}

	public void setCdSenha(String cdSenha) {
		this.cdSenha = cdSenha;
	}

}
