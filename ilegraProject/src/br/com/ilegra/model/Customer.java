package br.com.ilegra.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class Customer implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6480483961343758996L;
	
	private Integer customerId;
	
	private BigDecimal cnpj;
	
	private String nameBusiness;
	
	private String businessArea;

	/**
	 * @return the cnpj
	 */
	public BigDecimal getCnpj() {
		return cnpj;
	}

	/**
	 * @param cnpj the cnpj to set
	 */
	public void setCnpj(BigDecimal cnpj) {
		this.cnpj = cnpj;
	}

	/**
	 * @return the nameBusiness
	 */
	public String getNameBusiness() {
		return nameBusiness;
	}

	/**
	 * @param nameBusiness the nameBusiness to set
	 */
	public void setNameBusiness(String nameBusiness) {
		this.nameBusiness = nameBusiness;
	}

	/**
	 * @return the businessArea
	 */
	public String getBusinessArea() {
		return businessArea;
	}

	/**
	 * @param businessArea the businessArea to set
	 */
	public void setBusinessArea(String businessArea) {
		this.businessArea = businessArea;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	

}
