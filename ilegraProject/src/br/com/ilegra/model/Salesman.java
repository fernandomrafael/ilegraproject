package br.com.ilegra.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class Salesman implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 456500526464989268L;
	
	private Integer salesmanId;
	
	private BigDecimal cpf;
	
	private String name;
	
	private BigDecimal salary;

	/**
	 * @return the cpf
	 */
	public BigDecimal getCpf() {
		return cpf;
	}

	/**
	 * @param cpf the cpf to set
	 */
	public void setCpf(BigDecimal cpf) {
		this.cpf = cpf;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the salary
	 */
	public BigDecimal getSalary() {
		return salary;
	}

	/**
	 * @param salary the salary to set
	 */
	public void setSalary(BigDecimal salary) {
		this.salary = salary;
	}

	/**
	 * @return the salesmanId
	 */
	public Integer getSalesmanId() {
		return salesmanId;
	}

	/**
	 * @param salesmanId the salesmanId to set
	 */
	public void setSalesmanId(Integer salesmanId) {
		this.salesmanId = salesmanId;
	}
	
}
	
	

