package br.com.ilegra.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class Item implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 780147941079180377L;

	private Integer idItem;
	
	private Integer itemQtd;
	
	private BigDecimal itemPrice;

	/**
	 * @return the idItem
	 */
	public Integer getIdItem() {
		return idItem;
	}

	/**
	 * @param idItem the idItem to set
	 */
	public void setIdItem(Integer idItem) {
		this.idItem = idItem;
	}

	/**
	 * @return the itemPrice
	 */
	public BigDecimal getItemPrice() {
		return itemPrice;
	}

	/**
	 * @param itemPrice the itemPrice to set
	 */
	public void setItemPrice(BigDecimal itemPrice) {
		this.itemPrice = itemPrice;
	}

	/**
	 * @return the itemQtd
	 */
	public Integer getItemQtd() {
		return itemQtd;
	}

	/**
	 * @param itemQtd the itemQtd to set
	 */
	public void setItemQtd(Integer itemQtd) {
		this.itemQtd = itemQtd;
	}
	
	
	
	

}
