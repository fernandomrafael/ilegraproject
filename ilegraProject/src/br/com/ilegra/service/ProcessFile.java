package br.com.ilegra.service;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.ilegra.model.Item;
import br.com.ilegra.model.Sales;
import br.com.ilegra.model.Salesman;
import br.com.ilegra.vo.ItemVO;
import br.com.ilegra.vo.SalesVO;

@Service
@Transactional
public class ProcessFile {
	
	public void process(List<File> arquivosDevolucao){
		try {
			
			Integer countClients = 0;
			
			Integer countSalesman = 0;
			
			for (File arquivo : arquivosDevolucao) {
				
				String extensaoFile = FilenameUtils.getExtension(arquivo.getName());
				
				if (extensaoFile.equals("dat")) {
					System.out.println("Name arquivo: " + arquivo.getName());
					
					List<String> linhas = FileUtils.readLines(arquivo, "UTF-8");
					
					countClients = amountClients(linhas);
					
					countSalesman = amountSalesman(linhas);
					
					List<Salesman> worst = worstSalesman(linhas);
					
					Integer idVendaCaro = expensiveSale(linhas);
					
					String clints = "Clients: " + countClients;
					
					String salesman = "\nSalesman: " + countSalesman;
					
					String venda = "\nId Venda mais caro: " + idVendaCaro;
					
					String worstSalesman = "\nNome pior vendedor: " + worst.get(0).getName();
					
					String registro = clints + salesman + venda + worstSalesman; 
					
					readFiles(registro, arquivo.getName());
					
					System.out.println("Total de Vendedores: "+ registro);
					
					File backup = new File("C:\\Users\\Netpoints\\Desktop\\ilegra\\backup");
					
					FileUtils.copyFileToDirectory(arquivo, backup);
					
					//FileUtils.forceDelete(arquivo);
				}else {
					System.out.println("O Arquivo " + arquivo.getName() + " n�o ser� processado");
					
					File noProcessado = new File("C:\\Users\\Netpoints\\Desktop\\ilegra\\noProcess");
					
					FileUtils.copyFileToDirectory(arquivo, noProcessado);
					
					FileUtils.forceDelete(arquivo);
				}
				
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void readFiles(String registro, String fileName) throws IOException{
		
		String nameFile = FilenameUtils.getBaseName(fileName);
		
		File arquivo = new File("C:\\Users\\Netpoints\\Desktop\\ilegra\\out", nameFile +".done.dat");
		
		FileUtils.writeStringToFile(arquivo, registro, "UTF-8");	
		
	}
	
	public List<File> verifyFiles(String dir){
		
		File arquivosDevolucao = new File(dir);
		
		return Arrays.asList(arquivosDevolucao.listFiles());
		
	}
	
	//Verifica Quantidade de Clientes
	public Integer amountClients(List<String> linhas){
		String cnpj = "";
		
		int count = 0;
		
		for (String linha : linhas) {
			//Separa informa�oes
			String[] registro = linha.split("�");
			
			System.out.println("Linha: " + linha);
			
			if (registro[0].equals("002")) {
				
				if (cnpj.contains(registro[1])) {
					System.out.println("cnpj ja existe");
				}else{
					count++;	
				}
				cnpj = registro[1]+";"+cnpj;
			}
			
		}
		
		return count;
	}
	
	//Verifica Quantidade de Vendedores
	public Integer amountSalesman(List<String> linhas){
		
		String cpf = "";
		
		int count = 0;
		
		for (String linha : linhas) {
			//Separa informa�oes
			String[] registro = linha.split("�");
			
			System.out.println("Linha: " + linha);
			
			if (registro[0].equals("001")) {
				
				if (cpf.contains(registro[1])) {
					System.out.println("CPF ja existe");
				}else{
					count++;	
				}
				cpf = registro[1]+";"+cpf;
			}
			
		}
		
		return count;
	}
	
	//Verifica Id venda mais caro
	public Integer expensiveSale(List<String> linhas){
		
		List<Sales> sales = new ArrayList<Sales>();
		
		String salesId = "";
		
		Double maiorVenda = 0D;
		
		for (String linha : linhas) {
			
			//Separa informa�oes
			String[] registro = linha.split("�");
			
			System.out.println("Linha: " + linha);
			
			if (registro[0].equals("003")) {
				
				Double vendaTotal = 0D;
				
				Sales sale = new Sales();
				
				//Verifica se IdSales existe
				if (salesId.contains(registro[1])) {
					System.out.println("salesId ja existe");
				}else {
					Salesman salesman = new Salesman();
					
					sale.setSalesId(new Integer(registro[1]));
					
					salesman.setName(registro[3]);
					
					sale.setSalesman(salesman);
					
					List<Item> item = new ArrayList<Item>();
					
					String[] itensRegistro = registro[2].split(",");
					
					List<ItemVO> listItemVO = new ArrayList<ItemVO>();
					
					
					
					for (String listItens : itensRegistro) {
						Item newItem = new Item();
						
						//Remove caracteres [
						if (listItens.contains("[")) {
							listItens = listItens.replace("[", "");
						}
						//Remove caracteres ]
						if (listItens.contains("]")) {
							listItens = listItens.replace("]", "");
						}
						
						String[] splitItens = listItens.split("-");
						
						newItem.setIdItem(new Integer(splitItens[0]));
						newItem.setItemQtd(new Integer(splitItens[1]));
						newItem.setItemPrice(new BigDecimal(splitItens[2]));
						
						item.add(newItem);
					}
					
					

					//Realiza o parse do Model para o VO e calcula o total da venda
					for (Item itemParse : item) {
						
						ItemVO itemVO = new ItemVO();
						itemVO.setIdItem(itemParse.getIdItem());
						itemVO.setItemPrice(itemParse.getItemPrice());
						itemVO.setItemQtd(itemParse.getItemQtd());
						itemVO.setItemTotal(itemParse.getItemPrice().multiply(new BigDecimal(itemParse.getItemQtd())));
						listItemVO.add(itemVO);
						vendaTotal = itemVO.getItemTotal().doubleValue() + vendaTotal; 
						
					}
					
					Collections.sort(listItemVO);
					
					System.out.println("Maior venda " + listItemVO.get(0).getIdItem());
					
					System.out.println("Venda Total: " + vendaTotal);
					
				}
				//Variavel de verificao do IdSales
				salesId = registro[1]+";"+salesId;
				
				//Verifica total da venda
				if (vendaTotal >= maiorVenda) {
					maiorVenda = vendaTotal;
					sales.add(sale);
				}
			}
		}
		System.out.println("Maior venda: " + maiorVenda);
		
		System.out.println(sales.get(0).getSalesId());
		
		return sales.get(0).getSalesId();
	}
	
	//Verifica pior vendedor de cada vez
	public List<Salesman> worstSalesman(List<String> linhas){
		
		List<Salesman> listSalesman = new ArrayList<Salesman>();
		
		List<SalesVO> listSalesVO = new ArrayList<SalesVO>();
		
		String salesId = "";
		
		Double maiorVenda = 0D;
		
		for (String linha : linhas) {
			
			//Separa informa�oes
			String[] registro = linha.split("�");
			
			System.out.println("Linha: " + linha);
			
			if (registro[0].equals("003")) {
				
				Sales sale = new Sales();
				
				sale.setSalesId(new Integer(registro[1]));
				
				Salesman salesman = new Salesman();
				
				salesman.setName(registro[3]);
				
				sale.setSalesman(salesman);
				
				//Verifica se IdSales existe
				if (salesId.contains(registro[1])) {
					System.out.println("salesId ja existe");
				}else {
					
					List<Item> item = new ArrayList<Item>();
					
					String[] itensRegistro = registro[2].split(",");
					
					List<ItemVO> listItemVO = new ArrayList<ItemVO>();
					
					
					for (String listItens : itensRegistro) {
						Item newItem = new Item();
						
						//Remove caracteres [
						if (listItens.contains("[")) {
							listItens = listItens.replace("[", "");
						}
						//Remove caracteres ]
						if (listItens.contains("]")) {
							listItens = listItens.replace("]", "");
						}
						
						String[] splitItens = listItens.split("-");
						
						newItem.setIdItem(new Integer(splitItens[0]));
						newItem.setItemQtd(new Integer(splitItens[1]));
						newItem.setItemPrice(new BigDecimal(splitItens[2]));
						
						item.add(newItem);
					}

					//Realiza o parse do Model para o VO e calcula o total da venda
					for (Item itemParse : item) {
						
						ItemVO itemVO = new ItemVO();
						SalesVO saleVO = new SalesVO();
						itemVO.setIdItem(itemParse.getIdItem());
						itemVO.setItemPrice(itemParse.getItemPrice());
						itemVO.setItemQtd(itemParse.getItemQtd());
						itemVO.setItemTotal(itemParse.getItemPrice().multiply(new BigDecimal(itemParse.getItemQtd())));
						listItemVO.add(itemVO);
						maiorVenda = itemVO.getItemTotal().doubleValue() + maiorVenda; 
						//Parse sales for salesVO
						saleVO.setItens(sale.getItens());
						saleVO.setSalesId(sale.getSalesId());
						saleVO.setSalesman(sale.getSalesman());
						saleVO.setTotalSales(new BigDecimal(maiorVenda));
						listSalesVO.add(saleVO);
						
					}
					//Variavel de verificao do IdSales
					salesId = registro[1]+";"+salesId;
				}
			}
			Collections.sort(listSalesVO);
		}
		System.out.println(listSalesVO);
		
		for (SalesVO salesVO : listSalesVO) {
			
			Salesman salesman = new Salesman();

			salesman.setName(salesVO.getSalesman().getName());
			
			listSalesman.add(salesman);
			
		}
		
		return listSalesman;
		
	}

}
