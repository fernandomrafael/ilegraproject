package br.com.ilegra.notify;

import java.io.File;
import java.util.List;

import br.com.ilegra.service.ProcessFile;

public class Monitor implements Runnable{

	private Thread threadMonitor;  
    private String dir;  
  
    public static void main(String[] args) {  
        new Monitor().init();  
    }  
  
    private void init() {  
          
        // diretorio onde vai ser feito a verificacao  
        setDir("C:\\Users\\Netpoints\\Desktop\\ilegra\\in");  
          
        this.threadMonitor = new Thread(this);  
        this.threadMonitor.start();  
    }  
      
    private void verificaPasta(String dir) {  
        // lista todos os arquivos 
        ProcessFile processFile = new ProcessFile();
        
        List<File> arquivosDevolucao = processFile.verifyFiles(dir);
        
        if(arquivosDevolucao.size() != 0){
        	newFile(arquivosDevolucao);
        }
          
    }  
  
    public void newFile(List<File> files) {  
        System.out.println("evento -> Quantidade de arquivos encontrados: " + files.size());
        
        ProcessFile processFile = new ProcessFile();
        //Chama rotina de processamento
        processFile.process(files);
        
    }  
  
    @Override  
    public void run() {  
        Thread currentThread = Thread.currentThread();  
          
        // thread que fica verificando a pasta  
        while (this.threadMonitor == currentThread) {  
              
            verificaPasta(getDir());  
              
            try {  
                Thread.sleep(1000); // 10 segundos  
            } catch (InterruptedException e) {  
            }  
        }  
    }  
  
    public String getDir() {  
        return dir;  
    }  
  
    public void setDir(String dir) {  
        this.dir = dir;  
    }  
}
