package br.com.ilegra.data;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.Inheritance;

import org.hibernate.Criteria;
import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Example;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.transaction.annotation.Transactional;

@Inheritance
@Transactional
public abstract class GenericDaoImpl<T, ID extends Serializable> extends HibernateDaoSupport implements GenericDao<T, ID> {
	
	private Class<T> persistentClass;

	@SuppressWarnings("unchecked")
	public GenericDaoImpl() {
		this.persistentClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}

	public abstract void init(SessionFactory sessionFactory);
	
	public Class<T> getPersistentClass() {
		return persistentClass;
	}

	@SuppressWarnings({ "unchecked" })
	public T findById(ID id) {
		T entity;
		entity = (T) getSession().get(getPersistentClass(), id);
		return entity;
	}

	public List<T> findAll() {
		return findByCriteria();
	}

	@SuppressWarnings("unchecked")
	public List<T> findByExample(T exampleInstance, String[] excludeProperty) {
		Criteria crit = getSession().createCriteria(getPersistentClass());
		Example example = Example.create(exampleInstance);
		for (String exclude : excludeProperty) {
			example.excludeProperty(exclude);
		}
		crit.add(example);
		return crit.list();
	}
	
	@Override
	public <E> E makePersistent(E entity) {
		getSession().saveOrUpdate(entity);
		return entity;
	}

	@Override
	public <E> void makeTransient(E entity) {
		getSession().delete(entity);
	}
	
	public <E> void getLockEntity(E entity) {
		getSession().buildLockRequest(LockOptions.UPGRADE).lock(entity);
	}
	
	public void flush() {
		getSession().flush();
	}

	public void clear() {
		getSession().clear();
	}

	@SuppressWarnings("unchecked")
	public List<T> findByCriteria(Criterion... criterion) {
		Criteria crit = getSession().createCriteria(getPersistentClass());
		for (Criterion c : criterion) {
			crit.add(c);
		}
		return crit.list();
	}
	

	public void remove(T entity) {
		getSession().delete(entity);
		getSession().flush();
	}
	
	
	@Override
	public void removeById(ID... id) {		
		String hql  = "DELETE FROM "+ persistentClass.getCanonicalName() + " WHERE id in (:ids)" ;
		
		Query query = getSession().createQuery(hql);
		query.setParameterList("ids", id);		
		query.executeUpdate();
		getSession().flush();
	}
	
	private Criteria createCriteria() {
		return getSession().createCriteria(getPersistentClass());
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public T findSingleResultByCriteria(Criterion... criterion) {
		Criteria crit = createCriteria();
		for (Criterion c : criterion) {
			crit.add(c);
		}
		return (T) crit.uniqueResult();
	}
	
}
