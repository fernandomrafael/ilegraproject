package br.com.ilegra.data;

import java.io.Serializable;
import java.util.List;

import javax.persistence.criteria.Order;

import org.hibernate.criterion.Criterion;

public interface GenericDao<T, ID extends Serializable> {
	 
    T findById(ID id);
 
    Object findForPk(Class<?> clazz, Serializable pk) throws Exception;
    
    List<T> findAll();
    
    List<T> findByCriteria(Criterion... criterion);
    
    List<T> findByExample(T exampleInstance, String[] excludeProperty);
    
    List<T> findByCriteria(List<Criterion> criterion, List<Order> orders);
 
    <D> D makePersistent(D entity);
 
    <D> void makeTransient(D entity);

    void remove(T entity);
    
    void removeById(ID... id);
    
    <E> void getLockEntity(E entity);
    
    T findSingleResultByCriteria(Criterion... criterion);
}