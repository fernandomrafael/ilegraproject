package br.com.ilegra.vo;

import java.math.BigDecimal;
import java.util.ArrayList;

import br.com.ilegra.model.Item;
import br.com.ilegra.model.Salesman;

public class SalesVO implements Comparable<SalesVO>{
	
	private Integer salesId;
	
	private ArrayList<Item> itens;
	
	private Salesman salesman;
	
	private BigDecimal totalSales;

	/**
	 * @return the salesId
	 */
	public Integer getSalesId() {
		return salesId;
	}

	/**
	 * @param salesId the salesId to set
	 */
	public void setSalesId(Integer salesId) {
		this.salesId = salesId;
	}

	/**
	 * @return the itens
	 */
	public ArrayList<Item> getItens() {
		return itens;
	}

	/**
	 * @param itens the itens to set
	 */
	public void setItens(ArrayList<Item> itens) {
		this.itens = itens;
	}

	/**
	 * @return the salesman
	 */
	public Salesman getSalesman() {
		return salesman;
	}

	/**
	 * @param salesman the salesman to set
	 */
	public void setSalesman(Salesman salesman) {
		this.salesman = salesman;
	}

	/**
	 * @return the totalSales
	 */
	public BigDecimal getTotalSales() {
		return totalSales;
	}

	/**
	 * @param totalSales the totalSales to set
	 */
	public void setTotalSales(BigDecimal totalSales) {
		this.totalSales = totalSales;
	}
	
	@Override
	public int compareTo(SalesVO salesVO) {
		if (this.totalSales.doubleValue() > salesVO.totalSales.doubleValue()) {
            return -1;
        }
        if (this.totalSales.doubleValue() < salesVO.totalSales.doubleValue()) {
            return 1;
        }
        return 0;
	}
	
	
	

}
