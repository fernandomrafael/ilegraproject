package br.com.ilegra.vo;

import java.math.BigDecimal;

public class ItemVO implements Comparable<ItemVO>{
	
/**
	 * 
	 */

	private Integer idItem;
	
	private Integer itemQtd;
	
	private BigDecimal itemPrice;
	
	private BigDecimal itemTotal;

	/**
	 * @return the idItem
	 */
	public Integer getIdItem() {
		return idItem;
	}

	/**
	 * @param idItem the idItem to set
	 */
	public void setIdItem(Integer idItem) {
		this.idItem = idItem;
	}

	/**
	 * @return the itemQtd
	 */
	public Integer getItemQtd() {
		return itemQtd;
	}

	/**
	 * @param itemQtd the itemQtd to set
	 */
	public void setItemQtd(Integer itemQtd) {
		this.itemQtd = itemQtd;
	}

	/**
	 * @return the itemPrice
	 */
	public BigDecimal getItemPrice() {
		return itemPrice;
	}

	/**
	 * @param itemPrice the itemPrice to set
	 */
	public void setItemPrice(BigDecimal itemPrice) {
		this.itemPrice = itemPrice;
	}

	/**
	 * @return the itemTotal
	 */
	public BigDecimal getItemTotal() {
		return itemTotal;
	}

	/**
	 * @param itemTotal the itemTotal to set
	 */
	public void setItemTotal(BigDecimal itemTotal) {
		this.itemTotal = itemTotal;
	}

	@Override
	public int compareTo(ItemVO itemVO) {
		if (this.itemTotal.doubleValue() < itemVO.itemTotal.doubleValue()) {
            return -1;
        }
        if (this.itemTotal.doubleValue() > itemVO.itemTotal.doubleValue()) {
            return 1;
        }
        return 0;
	}

}
